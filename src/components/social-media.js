import React from "react";
import Linedkin from "../images/linkedin.svg";
import Twitter from "../images/twitter.svg";
import GitHub from "../images/github.svg"


const SocialMedia = () => {
  return (
    <section>
      {/*Linkedin*/}
      <a href="https://www.linkedin.com/in/matthew-hoare-frontdev/" target="__target" aria-label="Visit my Linkedin profile">
        <Linedkin/>
      </a>
      {/*Github*/}
      <a href="https://github.com/mattiehoare123" target="__target" aria-label="Visit my Github account">
        <GitHub/>
      </a>
      {/*Facebook*/}
      <a href="https://twitter.com/MattHoareWebDev" target="__target" aria-label="Visit my Twitter page">
        <Twitter/>
      </a>
    </section>
  )
}
export default SocialMedia
