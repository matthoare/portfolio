import React from "react"
import { Helmet } from "react-helmet";
import Layout from "../components/layout";
import Phone from "../images/cv/phone.svg";
import Mail from "../images/cv/mail.svg";
import Home from "../images/cv/home.svg";
import GitHub from "../images/cv/github.svg";
import LinkedIn from "../images/cv/linkedin.svg";

const CV = () => {
  return (
    <React.Fragment>
      <Helmet>
        <title>CV</title>
      </Helmet>
      <Layout>
        <h1 className="cv-title">CV</h1>
        <section class="cv-header">
          <section>
            <h2>
              Matthew Hoare
            </h2>
            <p>Front End Web Developer</p>
          </section>
          <ul>
            <li>
              <a href="tel:07887770657">
                <p>07887770657</p>
                <Phone/>
              </a>
            </li>
            <li>
              <a href="mailto:matthoare5@hotmail.co.uk?subject=Portfolio">
                <p>matthoare5@hotmail.co.uk</p>
                <Mail/>
              </a>
            </li>
            <li>
              <a href="https://www.google.com/maps?q=barberry+crescent+netherton&um=1&ie=UTF-8&sa=X&ved=2ahUKEwj8vabdzYbqAhUkTxUIHRFlBKIQ_AUoAXoECAwQAw">
                <p>Barberry Crescent, Merseyside</p>
                <Home/>
              </a>
            </li>
            <li>
              <a href="https://github.com/mattiehoare123" target="__target">
                <p>github.com/mattiehoare123</p>
                <GitHub/>
              </a>
            </li>
            <li>
              <a href="https://www.linkedin.com/in/matthew-hoare-frontdev/" target="__target">
                <p>linkedin.com/in/matthew-hoare-frontdev</p>
                <LinkedIn/>
              </a>
            </li>
          </ul>
        </section>
        <div class="cv-body">
          <section>
            <h2>Profile</h2>
            <p>
              {/*&#39; is for printing ' in text*/}
              I&#39;m a 3rd-year undergraduate student currently studying Web Design and Development and I
              am seeking an entry-level position in the front end sector. During my degree, I've developed
              excellent time management due to balancing my university workload while working at
              McDonald's. I proactively started a monthly internal employee presentation, which was
              recognised by the area manager and then rolled out region-wide. I&#39;ve also demonstrated I
              can work in a team and a fast-paced environment, as I've been on the production line whilst
              the store has broken numerous records. I&#39;m looking to start off my web career where I can
              show my passion for web development and learn new technologies along the way.
            </p>
          </section>
          <section>
            <h2>Key Skills</h2>
            <p>
              In my degree, I&#39;ve learnt the fundamentals of <span>HTML5</span>, <span>CSS3</span> and <span>JavaScript</span>. I&#39;ve used these
              languages to turn a design into a website with an API integrated. I&#39;ve built a CMS with the <span>PHP</span> framework <span>Laravel</span> and
              the database management system <span>MySQL</span>.
              The system was created through <span>BDD</span> testing and after each test had passed I used <span>Version Control</span> to push
              it to the repository. Additionally, I&#39;ve used the task runner <span>Gulp</span> to watch and format the
              SASS les automatically. I&#39;ve run <span>Usability Tests</span> with user's on a website and usability tests
              by myself, which included <span>Performance</span> and <span>Accessibility</span> testing. Furthermore, I&#39;ve
              conducted data analysis on web server logs through <span>Python</span> to identify issues users may be
              having on a website.
            </p>
          </section>
          <section>
            <h2>Education</h2>
            <section>
              <h3>Edge Hill University</h3>
              <p>September 2017 – July 2020</p>
              <p>BSc (Hons) Web Design & Development - Projected First Class Classification</p>
              <ul>
                <li>Implemented a high fidelity prototype into a website</li>
                <li>Created an interactive vending machine and connect four-game</li>
                <li>Designed and built a questionnaire system</li>
                <li>Conducted a series of usability tests with users and without</li>
                <li>Data Analysis on web server logs files</li>
                <li>Participated in a Milexa Group web challenge</li>
              </ul>
            </section>
            <section>
              <h3>The City of Liverpool College</h3>
              <p>September 2013 - July 2015</p>
              <p>Event Management - Merit</p>
              <ul>
                <li>Created a flyer for the Sefton Palm House wedding fair</li>
                <li>Front of house for R Charity Christmas carol concert</li>
              </ul>
            </section>
            <section>
              <h3>Maricourt Catholic High School</h3>
              <p>September 2008 - July 2013</p>
              <ul>
                <li>9 Passed GCSEs including Maths & English</li>
              </ul>
            </section>
          </section>
          <section>
            <h2>Experience</h2>
            <h3>McDonalds Switch Island</h3>
            <p>August 2016 – Present</p>
            <p>Crew Member</p>
            <ul>
              <li>Cook and prepare the food</li>
              <li>Take orders, payments and service of food to the customers</li>
              <li>Internal communications assistant for the store</li>
              <li>Been awarded Employee of the Month 3 times</li>
            </ul>
          </section>
          <section>
            <h2>Interests and Hobbies</h2>
            <ul>
              <li>I go to the gym on a regular basis and like to challenge myself in following a programme</li>
              <li>I have taught myself how to DJ and have too taught my friends</li>
              <li>I like taking coding courses and then implementing the features I've learnt by mocking up websites found on the web</li>
            </ul>
          </section>
        </div>
      </Layout>
    </React.Fragment>
  )
}
export default CV
